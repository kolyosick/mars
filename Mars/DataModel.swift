//
//  DataModel.swift
//  Mars
//
//  Created by n.alexeyev on 17.01.2018.
//

import UIKit
import Alamofire

protocol DataReceiverDelegate: class {
	func receivedData(_ object: AnyObject?)
	func receivedError(_ error: NSError)
}

class DataModel: NSObject {
	
	let baseUrl: String = "https://enigmatic-falls-31211.herokuapp.com/v1/"
	weak var delegate: DataReceiverDelegate? = nil
	let educationData = [String](arrayLiteral: "high", "middle", "school", "student")
	let marriedData = [String](arrayLiteral: "married", "civil marriage", "divorced", "single", "widower")
	
	func createForm() {
		Alamofire.request(baseUrl + "create/", method: .put, parameters: nil).responseJSON { response in
			switch response.result {
			case .success:
				if let JSON = response.result.value as? Dictionary<String, AnyObject>, let formId = JSON["id"] as? Int {
					UserDefaults.standard.set(formId, forKey: "formId")
					UserDefaults.standard.synchronize()
				}
			case .failure(let error):
				print(error)
				self.delegate?.receivedError(error as NSError)
			}
		}
	}
	
	func submitForm() {
		if UserDefaults.standard.object(forKey: "formId") == nil {
			return
		}
		let formId = UserDefaults.standard.object(forKey: "formId") as! Int
		
		let lastnameStored = UserDefaults.standard.object(forKey: "lastname") as! String
		let nameStored = UserDefaults.standard.object(forKey: "name") as! String
		let middlenameStored = UserDefaults.standard.object(forKey: "middlename") as! String
		let lastnameLatinStored = UserDefaults.standard.object(forKey: "lastnameLatin") as! String
		let nameLatinStored = UserDefaults.standard.object(forKey: "nameLatin") as! String
		
		var parameters = ["name" : nameStored,
						  "surname": lastnameStored,
						  "patronymic": middlenameStored,
						  "translated_name": nameLatinStored,
						  "translated_surname": lastnameLatinStored] as [String : Any]
		if let education = UserDefaults.standard.object(forKey: "education") as? Int {
			parameters["education"] = educationData[education]
		}
		if let married = UserDefaults.standard.object(forKey: "married") as? Int {
			parameters["marital_status"] = marriedData[married]
		}
		if let email = UserDefaults.standard.object(forKey: "email") as? String {
			parameters["email"] = email
		}
		if let phone = UserDefaults.standard.object(forKey: "phone") as? String {
			parameters["phone"] = phone
		}
		if let gender = UserDefaults.standard.object(forKey: "gender") as? String {
			parameters["gender"] = gender
		}
		if let date = UserDefaults.standard.object(forKey: "birthDate") as? Date {
			let birthDate = Int(date.timeIntervalSince1970)
			parameters["birthday"] = birthDate
		}
		if let lastnameOldStored = UserDefaults.standard.object(forKey: "lastnameOld") as? String {
			parameters["second_surname"] = lastnameOldStored
		}
		
		let url = baseUrl + "\(formId)" + "/submit/"
		Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
			print(response)
			switch response.result {
			case .success:
				if let JSON = response.result.value as? Dictionary<String, AnyObject>, let isDone = JSON["is_done"] as? Bool, isDone == true {
					self.delegate?.receivedData(nil)
				}
			case .failure(let error):
				print(error)
				self.delegate?.receivedError(error as NSError)
			}
		}
	}
	
	func recognizeGender() {
		let lastnameStored = UserDefaults.standard.object(forKey: "lastname") as! String
		let nameStored = UserDefaults.standard.object(forKey: "name") as! String
		
		let parameters = ["name" : nameStored,
						  "surname": lastnameStored]
		Alamofire.request(baseUrl + "gender/", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
			switch response.result {
			case .success:
				if let JSON = response.result.value as? Dictionary<String, AnyObject>, let gender = JSON["gender"] {
					if gender is String {
						UserDefaults.standard.set(gender, forKey: "gender")
						UserDefaults.standard.synchronize()
					}
					self.delegate?.receivedData(gender)
				}
			case .failure(let error):
				print(error)
				self.delegate?.receivedError(error as NSError)
			}
		}
	}

}
