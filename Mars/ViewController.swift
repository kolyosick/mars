//
//  ViewController.swift
//  Mars
//
//  Created by n.alexeyev on 15.01.2018.
//

import UIKit
import ValidationComponents
import PhoneNumberKit

class ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, DataReceiverDelegate {
	
	let model = DataModel()
	var lastnameChanged = false
	var agreed = false
	var genderPicker = UIPickerView()
	var datePicker = UIDatePicker()
	var educationPicker = UIPickerView()
	var marriedPicker = UIPickerView()
	let educationPickerData = [String](arrayLiteral: "высшее", "среднее специальное", "среднее", "студент")
	let phoneNumberKit = PhoneNumberKit()
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var sentView: UIView!
	@IBOutlet weak var blurView: UIVisualEffectView!
	@IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
	@IBOutlet weak var lastname: UITextField!
	@IBOutlet weak var lastnameOld: UITextField!
	@IBOutlet weak var lastnameOldHeight: NSLayoutConstraint!
	@IBOutlet weak var lastnameOldButton: UIButton!
	@IBOutlet weak var name: UITextField!
	@IBOutlet weak var middlename: UITextField!
	@IBOutlet weak var lastnameLatin: UITextField!
	@IBOutlet weak var nameLatin: UITextField!
	@IBOutlet weak var genderField: UITextField!
	@IBOutlet weak var birthDateField: UITextField!
	@IBOutlet weak var marriedField: UITextField!
	@IBOutlet weak var educationField: UITextField!
	@IBOutlet weak var phoneField: PhoneNumberTextField!
	@IBOutlet weak var emailField: UITextField!
	@IBOutlet weak var agreementButton: UIButton!
	@IBOutlet weak var sendButton: UIButton!
	@IBOutlet weak var createNewFormButton: UIButton!
	@IBOutlet weak var activity: UIActivityIndicatorView!

	override func viewDidLoad() {
		super.viewDidLoad()
		
		model.delegate = self
		
		if UserDefaults.standard.object(forKey: "formId") == nil {
			model.createForm()
		}
		
		let tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(touchAnywhere))
		tapGestureRecognizer.numberOfTapsRequired = 1
		view.addGestureRecognizer(tapGestureRecognizer)
		
		createNewFormButton.layer.borderColor = createNewFormButton.titleLabel?.textColor.cgColor
		
		makeGenderPicker()
		makeMarriedPicker()
		makeEducationPicker()
		makeDatePicker()
		setupFields()
		
		updateSendButtonState()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		registerKeyboardNotifications()
	}
	
	func registerKeyboardNotifications() {

		NotificationCenter.default.addObserver(self,
											   selector: #selector(keyboardWillShow(notification:)),
											   name: NSNotification.Name.UIKeyboardWillShow,
											   object: nil)
		NotificationCenter.default.addObserver(self,
											   selector: #selector(keyboardWillHide(notification:)),
											   name: NSNotification.Name.UIKeyboardWillHide,
											   object: nil)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		NotificationCenter.default.removeObserver(self)
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	// MARK: - Actions
	
	@objc func touchAnywhere() {
		view.endEditing(true)
	}
	
	@IBAction func toggleLastnameOldButton() {
		lastnameChanged = !lastnameChanged
		UserDefaults.standard.set(lastnameChanged, forKey: "lastnameChanged")
		updateLastnameChangedState()
		updateSendButtonState()
	}
	
	@IBAction func toggleAgreementButton() {
		agreed = !agreed
		UserDefaults.standard.set(agreed, forKey: "agreed")
		updateAgreedState()
	}
	
	@IBAction func send() {
		view.endEditing(true)
		model.submitForm()
		sendButton.setTitle(nil, for: .normal)
		activity.startAnimating()
	}
	
	@IBAction func newForm() {
		model.createForm()
		
		resetFields()
		setupFields()
		self.scrollView.scrollRectToVisible(CGRect(x:0, y:0, width: 1.0, height: 1.0), animated: false)
		
		UIView.animate(withDuration: 0.3, animations: {
			self.scrollView.alpha = 1.0
			self.sentView.alpha = 0
			self.blurView.effect = UIBlurEffect(style: .dark)
		}, completion: { (completed) in
			self.sentView.isHidden = true
		})
	}
	
	// MARK: -
	
	func setupFields() {
		if let date = UserDefaults.standard.object(forKey: "birthDate") as? Date {
			datePicker.date = date
			handleDatePicker(sender: datePicker)
		} else {
			datePicker.date = Date()
			birthDateField.text = nil
		}
		
		if let gender = UserDefaults.standard.object(forKey: "gender") as? String {
			genderField.text = gender == "male" ? "Мужчина" : "Женщина"
			genderPicker.selectRow(gender == "male" ? 0 : 1, inComponent: 0, animated: false)
		} else {
			genderField.text = nil
			genderPicker.selectRow(0, inComponent: 0, animated: false)
		}
		
		lastnameChanged = UserDefaults.standard.bool(forKey: "lastnameChanged")
		UIView.performWithoutAnimation {
			updateLastnameChangedState()
		}
		agreed = UserDefaults.standard.bool(forKey: "agreed")
		updateAgreedState()
		
		if let lastnameStored = UserDefaults.standard.object(forKey: "lastname") as? String {
			lastname.text = lastnameStored
		} else {
			lastname.text = nil
		}
		if let lastnameOldStored = UserDefaults.standard.object(forKey: "lastnameOld") as? String {
			lastnameOld.text = lastnameOldStored
		} else {
			lastnameOld.text = nil
		}
		if let nameStored = UserDefaults.standard.object(forKey: "name") as? String {
			name.text = nameStored
		} else {
			name.text = nil
		}
		if let middlenameStored = UserDefaults.standard.object(forKey: "middlename") as? String {
			middlename.text = middlenameStored
		} else {
			middlename.text = nil
		}
		if let lastnameLatinStored = UserDefaults.standard.object(forKey: "lastnameLatin") as? String {
			lastnameLatin.text = lastnameLatinStored
		} else {
			lastnameLatin.text = nil
		}
		if let nameLatinStored = UserDefaults.standard.object(forKey: "nameLatin") as? String {
			nameLatin.text = nameLatinStored
		} else {
			nameLatin.text = nil
		}
		if let education = UserDefaults.standard.object(forKey: "education") as? Int {
			educationPicker.selectRow(education + 1, inComponent: 0, animated: false)
			educationField.text = educationPickerData[education]
		} else {
			educationPicker.selectRow(0, inComponent: 0, animated: false)
			educationField.text = nil
		}
		setupMarriedField()
		if let email = UserDefaults.standard.object(forKey: "email") as? String {
			emailField.text = email
		} else {
			emailField.text = nil
		}
		if let phone = UserDefaults.standard.object(forKey: "phone") as? String {
			phoneField.text = phone
		} else {
			phoneField.text = nil
		}
	}
	
	func resetFields() {
		UserDefaults.standard.removeObject(forKey: "birthDate")
		UserDefaults.standard.removeObject(forKey: "lastnameChanged")
		UserDefaults.standard.removeObject(forKey: "agreed")
		UserDefaults.standard.removeObject(forKey: "lastname")
		UserDefaults.standard.removeObject(forKey: "lastnameOld")
		UserDefaults.standard.removeObject(forKey: "name")
		UserDefaults.standard.removeObject(forKey: "middlename")
		UserDefaults.standard.removeObject(forKey: "lastnameLatin")
		UserDefaults.standard.removeObject(forKey: "nameLatin")
		UserDefaults.standard.removeObject(forKey: "education")
		UserDefaults.standard.removeObject(forKey: "married")
		UserDefaults.standard.removeObject(forKey: "email")
		UserDefaults.standard.removeObject(forKey: "phone")
		UserDefaults.standard.removeObject(forKey: "gender")
		UserDefaults.standard.synchronize()
	}
	
	func setupMarriedField() {
		if let married = UserDefaults.standard.object(forKey: "married") as? Int {
			marriedPicker.selectRow(married + 1, inComponent: 0, animated: false)
			marriedField.text = getMarriedPickerData()[married]
		} else {
			marriedPicker.selectRow(0, inComponent: 0, animated: false)
			marriedField.text = nil
		}
	}
	
	func updateSendButtonState() {
		if let lasnnameOldText = lastnameOld.text, lasnnameOldText.count == 0 && lastnameOldButton.isSelected {
			sendButton.backgroundColor = UIColor(hexString: "423f3c")
			sendButton.isEnabled = false
			sendButton.setTitleColor(UIColor(hexString: "555352"), for: .normal)
		} else if let lastnameText = lastname.text, lastnameText.count > 0, let nameText = name.text, nameText.count > 0, let middlenameText = middlename.text, middlenameText.count > 0, let nameLatinText = nameLatin.text, nameLatinText.count > 0, let lastnameLatinText = lastnameLatin.text, lastnameLatinText.count > 0, let birthText = birthDateField.text, birthText.count > 0, let educationText = educationField.text, educationText.count > 0, let marriedText = marriedField.text, marriedText.count > 0, let emailText = emailField.text, emailText.count > 0, let phoneText = phoneField.text, phoneText.count > 0, agreementButton.isSelected {
			sendButton.backgroundColor = UIColor(hexString: "FF4800")
			sendButton.isEnabled = true
			sendButton.setTitleColor(UIColor.white, for: .normal)
		} else {
			sendButton.backgroundColor = UIColor(hexString: "423f3c")
			sendButton.isEnabled = false
			sendButton.setTitleColor(UIColor(hexString: "555352"), for: .normal)
		}
	}
	
	func updateLastnameChangedState() {
		if lastnameChanged {
			lastnameOldHeight.constant = 59.0
			lastnameOldButton.isSelected = true
		} else {
			lastnameOldHeight.constant = 0
			lastnameOldButton.isSelected = false
		}
		UIView.animate(withDuration: 0.25) {
			self.view.layoutIfNeeded()
		}
	}
	
	func updateAgreedState() {
		if agreed {
			agreementButton.isSelected = true
		} else {
			agreementButton.isSelected = false
		}
		updateSendButtonState()
	}
	
	func makeGenderPicker() {
		genderPicker.delegate = self
		genderField.inputView = genderPicker
	}
	
	func makeMarriedPicker() {
		marriedPicker.delegate = self
		marriedField.inputView = marriedPicker
	}
	
	func makeEducationPicker() {
		educationPicker.delegate = self
		educationField.inputView = educationPicker
	}
	
	func makeLatinLastname() {
		let string = lastname.text
		let latinString = string?.applyingTransform(StringTransform.toLatin, reverse: false)
		let noDiacriticString = latinString?.applyingTransform(StringTransform.stripDiacritics, reverse: false)
		lastnameLatin.text = noDiacriticString
		UserDefaults.standard.set(lastnameLatin.text, forKey: "lastnameLatin")
	}
	
	func makeLatinName() {
		let string = name.text
		let latinString = string?.applyingTransform(StringTransform.toLatin, reverse: false)
		let noDiacriticString = latinString?.applyingTransform(StringTransform.stripDiacritics, reverse: false)
		nameLatin.text = noDiacriticString
		UserDefaults.standard.set(nameLatin.text, forKey: "nameLatin")
	}
	
	// MARK: - Date picker
	
	func makeDatePicker() {
		let date18ago = Calendar.current.date(byAdding: .year, value: -18, to: Date())
		let date100ago = Calendar.current.date(byAdding: .year, value: -100, to: Date())
		
		datePicker.datePickerMode = .date
		datePicker.maximumDate = date18ago
		datePicker.minimumDate = date100ago
		datePicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
		
		birthDateField.inputView = datePicker
	}
	
	@objc func handleDatePicker(sender: UIDatePicker) {
		let formatter = DateFormatter()
		formatter.dateStyle = .medium
		birthDateField.text = formatter.string(from: sender.date)
		UserDefaults.standard.set(sender.date, forKey: "birthDate")
		UserDefaults.standard.synchronize()
		updateSendButtonState()
	}
	
	// MARK: - UIPickerViewDelegate
	
	public func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}
	
	public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		if pickerView == marriedPicker {
			return getMarriedPickerData().count + 1
		} else if pickerView == educationPicker {
			return educationPickerData.count + 1
		}
		return 2
	}
	
	func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		if pickerView == marriedPicker {
			if row == 0 {
				return nil
			}
			return getMarriedPickerData()[row - 1]
		} else if pickerView == educationPicker {
			if row == 0 {
				return nil
			}
			return educationPickerData[row - 1]
		}
		return row == 0 ? "Мужчина" : "Женщина"
	}
	
	func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		if pickerView == marriedPicker {
			if row == 0 {
				marriedField.text = nil
				UserDefaults.standard.removeObject(forKey: "married")
			} else {
				marriedField.text = getMarriedPickerData()[row - 1]
				UserDefaults.standard.set(row - 1, forKey: "married")
			}
		} else if pickerView == educationPicker {
			if row == 0 {
				educationField.text = nil
				UserDefaults.standard.removeObject(forKey: "education")
			} else {
				educationField.text = educationPickerData[row - 1]
				UserDefaults.standard.set(row - 1, forKey: "education")
			}
		} else {
			genderField.text = row == 0 ? "Мужчина" : "Женщина"
			UserDefaults.standard.set(row == 0 ? "male" : "female", forKey: "gender")
			setupMarriedField()
		}
		UserDefaults.standard.synchronize()
		updateSendButtonState()
	}
	
	// MARK: - UITextFieldDelegate
	
	public func textFieldDidBeginEditing(_ textField: UITextField) {
		if textField is TextField {
			textField.tintColor = UIColor.white
		}
	}
	
	public func textFieldDidEndEditing(_ textField: UITextField) {
		textField.tintColor = UIColor.white.withAlphaComponent(0.3)
		
		if textField == lastname {
			UserDefaults.standard.set(lastname.text, forKey: "lastname")
			if UserDefaults.standard.bool(forKey: "lastnameLatinChanged") == false {
				makeLatinLastname()
			}
		} else if textField == lastnameOld {
			UserDefaults.standard.set(lastnameOld.text, forKey: "lastnameOld")
		} else if textField == name {
			UserDefaults.standard.set(name.text, forKey: "name")
			if UserDefaults.standard.bool(forKey: "nameLatinChanged") == false {
				makeLatinName()
			}
			if UserDefaults.standard.object(forKey: "lastname") != nil {
				model.recognizeGender()
			}
		} else if textField == middlename {
			UserDefaults.standard.set(middlename.text, forKey: "middlename")
		} else if textField == lastnameLatin {
			UserDefaults.standard.set(lastnameLatin.text, forKey: "lastnameLatin")
			if lastnameLatin.text?.count == 0 {
				UserDefaults.standard.set(false, forKey: "lastnameLatinChanged")
				UserDefaults.standard.synchronize()
				makeLatinLastname()
			}
		} else if textField == nameLatin {
			UserDefaults.standard.set(nameLatin.text, forKey: "nameLatin")
			if nameLatin.text?.count == 0 {
				UserDefaults.standard.set(false, forKey: "nameLatinChanged")
				UserDefaults.standard.synchronize()
				makeLatinName()
			}
		} else if textField == emailField {
			UserDefaults.standard.set(emailField.text, forKey: "email")
			let predicate = EmailValidationPredicate()
			if !predicate.evaluate(with: textField.text) {
				textField.tintColor = UIColor.red
			}
		} else if textField == phoneField {
			if let text = phoneField.text, text.count > 0 {
				UserDefaults.standard.set(text, forKey: "phone")
				textField.tintColor = UIColor.white.withAlphaComponent(0.3)
			} else {
				textField.tintColor = UIColor.red
			}
		}
		UserDefaults.standard.synchronize()
		updateSendButtonState()
	}
	
	public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField == lastname {
			name.becomeFirstResponder()
		} else if textField == lastnameOld {
			name.becomeFirstResponder()
		} else if textField == name {
			middlename.becomeFirstResponder()
		} else if textField == middlename {
			birthDateField.becomeFirstResponder()
		} else if textField == lastnameLatin {
			nameLatin.becomeFirstResponder()
		} else if textField == nameLatin {
			birthDateField.becomeFirstResponder()
		} else {
			textField.resignFirstResponder()
		}
		updateSendButtonState()
		return false
	}
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		if string.isEmpty {
			if textField == lastnameLatin || textField == nameLatin {
				setLatinFieldChanged(textField)
			}
			return true
		}
		if textField == name || textField == lastname || textField == middlename || textField == lastnameOld {
			let alphaNumericRegEx = "[а-яА-Я\\-]+"
			let predicate = NSPredicate(format:"SELF MATCHES %@", alphaNumericRegEx)
			return predicate.evaluate(with: string)
		}
		if textField != lastnameLatin && textField != nameLatin {
			return true
		}
		let alphaNumericRegEx = "[a-zA-Z\\-]+"
		let predicate = NSPredicate(format:"SELF MATCHES %@", alphaNumericRegEx)
		let isOkay = predicate.evaluate(with: string)
		if isOkay {
			setLatinFieldChanged(textField)
		}
		return isOkay
	}
	
	func setLatinFieldChanged(_ textField: UITextField) {
		UserDefaults.standard.set(true, forKey: (textField == lastnameLatin ? "lastnameLatinChanged" : "nameLatinChanged"))
		UserDefaults.standard.synchronize()
	}
	
	// MARK: - Notifications
	
	@objc func keyboardWillShow(notification: NSNotification) {
		updateBottomLayoutConstraintWithNotification(notification: notification)
	}
	
	@objc func keyboardWillHide(notification: NSNotification) {
		updateBottomLayoutConstraintWithNotification(notification: notification)
	}
	
	
	// MARK: - Private
	
	func getMarriedPickerData() -> [String] {
		if let gender = UserDefaults.standard.object(forKey: "gender") as? String  {
			if gender == "male" {
				return Array(arrayLiteral: "женат", "в «гражданском браке»", "разведён", "холост", "вдовец")
			} else if gender == "female" {
				return Array(arrayLiteral: "замужем", "в «гражданском браке»", "разведена", "не замужем", "вдова")
			}
		}
		return Array(arrayLiteral: "женат/замужем", "в «гражданском браке»", "разведён", "холост", "вдовец")
	}
	
	func updateBottomLayoutConstraintWithNotification(notification: NSNotification) {
		let userInfo = notification.userInfo!
		let animationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
		let keyboardEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
		let convertedKeyboardEndFrame = view.convert(keyboardEndFrame, from: view.window)
		let rawAnimationCurve = (notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).uint32Value << 16
		let animationCurve = UIViewAnimationOptions(rawValue: UInt(rawAnimationCurve))
		bottomLayoutConstraint.constant = (view.bounds).maxY - (convertedKeyboardEndFrame).minY
		
		UIView.animate(withDuration: animationDuration, delay: 0.0, options: [.beginFromCurrentState, animationCurve], animations: {
			self.view.layoutIfNeeded()
		}, completion: nil)
	}
	
	func restoreSendButtonTitle() {
		sendButton.setTitle("Полететь на Марс", for: .normal)
	}
	
	// MARK: - DataReceiverDelegate
	
	func receivedData(_ object: AnyObject?) {
		if object == nil {
			self.sentView.isHidden = false
			UIView.animate(withDuration: 0.3, animations: {
				self.scrollView.alpha = 0
				self.sentView.alpha = 1.0
				self.blurView.effect = nil
			})
		} else if object is String {
			genderField.text = (object as! String) == "male" ? "Мужчина" : "Женщина"
			genderPicker.selectRow((object as! String) == "male" ? 0 : 1, inComponent: 0, animated: false)
			UserDefaults.standard.set(object, forKey: "gender")
			UserDefaults.standard.synchronize()
			setupMarriedField()
		}
		activity.stopAnimating()
		restoreSendButtonTitle()
	}
	
	func receivedError(_ error: NSError) {
		let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
		alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
			alert.dismiss(animated: true, completion: nil)
		}))
		self.present(alert, animated: true, completion: nil)
		activity.stopAnimating()
		restoreSendButtonTitle()
	}
	
}

